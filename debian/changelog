kraft (1.2.1-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Fri, 05 Jul 2024 14:05:33 +1000

kraft (1.1-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16.0
  * Pass -DAKONADI_LEGACY_BUILD=ON to cmake to build with Akonadi older than
    23.04 (i.e. what currently available in Debian unstable).
  * Fix source build without git or a .git directory around:
    - do not try to get git branch information if there is no .git directory;
      patch cmake-fix-without-git.diff
    - create a ".tag" file for the cmake run, since apparently the build system
      assumes it is there (while it is not) in case .git is not present
  * Make the tests pass:
    - add the locales-all build dependency
    - run tests using the de_DE.utf8 locale
    - fix the expected date strings when no date format is configured; patch
      tests-dateformat.diff

 -- Pino Toscano <pino@debian.org>  Sun, 17 Sep 2023 06:01:17 +0200

kraft (1.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes required.
  * Update the build dependencies according to the upstream build system:
    - add libqt5svg5-dev
  * Remove inactive Uploaders, adding myself as one to avoid leaving the source
    with no human maintainers.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 23 Dec 2022 14:47:38 +0100

kraft (0.98-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.1, no changes required.
  * Update the patches:
    - fix_build_KDEPIM.21.12.patch: drop, fixed upstream
  * Mark the xauth, and xvfb build dependencies as <!nocheck>, as they are
    needed only when running the test suite.
  * Skip the test t_defaultprovider for now; patch
    tests-skip-t_defaultprovider.diff.

 -- Pino Toscano <pino@debian.org>  Mon, 23 May 2022 09:51:20 +0200

kraft (0.97-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Team upload.
  * Add patch to fix build failures against KDEPIM 21.12.

 -- Patrick Franz <deltaone@debian.org>  Sat, 02 Apr 2022 15:25:56 +0200

kraft (0.97-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 21 Aug 2021 08:31:23 +0200

kraft (0.96-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes required.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Pino Toscano <pino@debian.org>  Sun, 28 Feb 2021 11:20:27 +0100

kraft (0.95-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the patches:
    - upstream_appstream-fix-translation.patch: drop, backported from upstream
    - upstream_Check-for-only-for-Python-3-for-erml2pdf.py.patch: drop,
      backported from upstream
  * Update the build dependencies according to the upstream build system:
    - add libgrantlee5-dev, used for templates
    - add asciidoctor, and po4a, used for generating the HTML documentation
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
  * Do not run the test suite in parallel, as it seems there are conflicts
    between tests.
  * Recommend weasyprint for PDF reports.

 -- Pino Toscano <pino@debian.org>  Sat, 29 Aug 2020 07:40:06 +0200

kraft (0.90-2) unstable; urgency=medium

  * Team upload.
  * Use dh_python3 to calculate automatically the Python 3 dependency:
    - add the dh-python, and python3:any build dependencies
    - use the python3 dh addon
    - use the ${python3:Depends} substvar, removing the manual python3:any
      dependency
  * Properly run the unit tests, so they work:
    - add the libqt5sql5-sqlite, xauth, and xvfb build dependencies
    - run dh_auto_test with xvfb-run, and set the KRAFT_HOME environment
      variable as documented
  * Backport upstream commit 5b516b35b65d6f00280ba3f6fe4eebd564e02102 to fix
    <translation> in the appstream file; patch
    upstream_appstream-fix-translation.patch.
  * Backport upstream commit 7eff90f462c740b527a1484e31383ea707351180 to switch
    to Python 3, replacing the existing patch python3.diff; patch
    upstream_Check-for-only-for-Python-3-for-erml2pdf.py.patch.

 -- Pino Toscano <pino@debian.org>  Wed, 01 Jan 2020 21:24:48 +0100

kraft (0.90-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add the configuration for the CI on salsa.
  * Update the build dependencies according to the upstream build system:
    - remove libkf5coreaddons-dev, libkf5widgetsaddons-dev, and
      libkf5xmlgui-dev
    - add libkf5config-dev
    - explicitly add gettext
  * The new unit tests do not seem to work with an uninstalled kraft, so
    do not invoke dh_auto_test.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Drop the man page, as it is mostly obsolete.
  * Switch the erml2pdf.py invocation to Python 3: (Closes: #945687, #935190)
    - replace the python-reportlab, python-six, and python-pypdf2 dependencies
      with python3-reportlab, python3-six, and python3-pypdf2
    - explicitly depend on python3:any
    - make sure to only look for 'python3' as Python interpreter;
      patch python3.diff

 -- Pino Toscano <pino@debian.org>  Sun, 15 Dec 2019 10:37:17 +0100

kraft (0.82-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.2.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 20 Oct 2018 10:15:25 +0200

kraft (0.81-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the patches:
    - upstream_Fix-install-directory-for-appdata.patch: drop, backported from
      upstream
    - upstream_Update-appstream-metadata-to-the-latest-spec.patch: drop,
      backported from upstream
    - upstream_Modifications-to-the-appdata-xml-file.-Added-missing.patch:
      drop, backported from upstream
  * Update Vcs-* fields.

 -- Pino Toscano <pino@debian.org>  Sat, 16 Jun 2018 09:06:25 +0200

kraft (0.80-2) unstable; urgency=medium

  * Team upload.
  * Remove the pdftk dependency, as it is not required since kraft 0.40.
  * Since kraft embeds the trml2pdf Python module as script, replace the
    python-trml2pdf dependency with python-reportlab, and python-six.

 -- Pino Toscano <pino@debian.org>  Wed, 16 May 2018 06:00:55 +0200

kraft (0.80-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - switches to Qt5/KF5 (Closes: #874993)
  * Update watch file to point to the GitHub location.
  * Update the build dependencies following the port to Frameworks:
    - remove kdelibs5-dev, and kdepimlibs5-dev
    - add extra-cmake-modules, qtbase5-dev, libkf5akonadi-dev,
      libkf5akonadicontact-dev, libkf5contacts-dev, libkf5coreaddons-dev,
      libkf5i18n-dev, libkf5widgetsaddons-dev, and libkf5xmlgui-dev
  * Use the right dh addon:
    - switch from kde to kf5 dh addon
    - bump the pkg-kde-tools build dependency to >= 0.15.16
  * Update the runtime dependencies/recommends:
    - replace libqt4-sql-sqlite with libqt5sql5-sqlite
    - replace libqt4-sql-mysql with libqt5sql5-mysql
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Update the patches:
    - PyPDF2.patch: drop, fixed upstream
  * Remove trailing whitespaces in changelog.
  * Remove unused libboost-dev build dependency.
  * Bump Standards-Version to 4.1.4, no changes required.
  * Switch Vcs-* fields to salsa.debian.org.
  * Backport upstream commits 0001af5a70a78aeceb9745fdaac9bfdc0245543e,
    e113781933f814e9394bf9bbb8d9ecb9572b88e5, and
    673722084bf75c9be154802427377ebf6ec853ad to make the appdata file valid,
    and install it in the correct location; patches
    upstream_Fix-install-directory-for-appdata.patch,
    upstream_Update-appstream-metadata-to-the-latest-spec.patch, and
    upstream_Modifications-to-the-appdata-xml-file.-Added-missing.patch.

 -- Pino Toscano <pino@debian.org>  Fri, 11 May 2018 06:10:25 +0200

kraft (0.59-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * python-pypdf2 transition Closes: #763980.

 -- Luciano Bello <luciano@debian.org>  Thu, 21 Jul 2016 15:29:36 +0200

kraft (0.59-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove symlinks cleanup in rules, as there are no more shared libraries
    built.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Update watch file.
  * Remove unused build dependencies: nepomuk-core-dev, libqt4-sql-sqlite.

 -- Pino Toscano <pino@debian.org>  Sat, 12 Dec 2015 16:08:30 +0100

kraft (0.55-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 31 May 2014 10:07:40 +1000

kraft (0.53-2) unstable; urgency=low

  * Team upload.
  * Drop kplant man page, since that executable does not exist anymore.
  * Drop kraft.dirs, useless now.
  * Link in as-needed mode.
  * Remove .so symlinks from the binary package.
  * Bump Standards-Version to 3.9.5, no changes required.
  * Update kraft man page.

 -- Pino Toscano <pino@debian.org>  Sun, 05 Jan 2014 09:48:40 +0100

kraft (0.53-1) unstable; urgency=low

  * New upstream release
  * Drop 01_dont_install_cmake_script.patch - included upstream

 -- Mark Purcell <msp@debian.org>  Sun, 13 Oct 2013 12:59:51 +1100

kraft (0.51-1) unstable; urgency=low

  * New upstream release
  * Add Build-Depends: nepomuk-core-dev

 -- Mark Purcell <msp@debian.org>  Sat, 28 Sep 2013 06:48:16 +1000

kraft (0.50-2) unstable; urgency=low

  * Upload to unstable
  * Update debian/control - fix vcs-field-not-canonical
  * Update debian/copyright - fix copyright-refers-to-symlink-license
  * Update Standards-Version: 3.9.4 - no changes

 -- Mark Purcell <msp@debian.org>  Sun, 12 May 2013 09:43:36 +1000

kraft (0.50-1) experimental; urgency=low

  * New upstream release
  * debian/compat -> 9
  * Added Depends: python-pypdf
  * Updated Description:
    - Fixes "Please explain what it does in the description" (Closes: #678934)

 -- Mark Purcell <msp@debian.org>  Sun, 23 Dec 2012 12:49:08 +1100

kraft (0.45-2) unstable; urgency=low

  [ Mark Purcell ]
  * Update Build-Depends: libboost-dev
    - Fixes "update boost build-dep" (Closes: #672754)

  [ Felix Geyer ]
  * Remove myself from Uploaders.
  * Fix Vcs-Browser url.

 -- Mark Purcell <msp@debian.org>  Sun, 10 Jun 2012 15:23:33 +1000

kraft (0.45-1) unstable; urgency=low

  * New upstream release
    - Fixes "[kraft] New upstream release:" (Closes: #647890)
  * Drop 03_fix_ftbfs_no-add-needed.patch, included upstream

 -- Mark Purcell <msp@debian.org>  Sun, 13 Nov 2011 08:40:16 +1100

kraft (0.43-1) unstable; urgency=low

  * New upstream release

  [ Mark Purcell ]
  * Update debian/copyright
  * Update Build-Depends: libboost1.46-dev
  * Drop 02_fix_desktop_file.patch - fixed upstream
  * Drop private icons folder - fixed upstream

  [ Felix Geyer ]
  * Add 03_fix_ftbfs_no-add-needed.patch to link kraft against libakonadi-kde.
    (Closes: #615737)

 -- Mark Purcell <msp@debian.org>  Fri, 10 Jun 2011 17:28:53 +1000

kraft (0.40-1) unstable; urgency=low

  [ Mark Purcell ]
  * Initial Upload to Debian (Closes: #580718)
  * Maintainer: Debian KDE Extras Team & add myself to Uploaders
  * Switch to dh 7

  [ Felix Geyer ]
  * Depend on libqt4-sql-sqlite and recommend libqt4-sql-mysql.
  * Update debian/copyright.
  * Add watch file.
  * Change homepage to http://volle-kraft-voraus.de/.
  * Add cmake to build-deps and drop docbook2x.
  * Enable parallel building.
  * Add 01_dont_install_cmake_script.patch to prevent Kraft from installing
    FindCtemplate.cmake.
  * Add 02_fix_desktop_file.patch to remove duplicate "Encoding" entries.
  * Add Vcs fields in debian/control.
  * Add manpages for kraft and kplant.
  * Install demand.png and alternative.png to private icons dir to prevent
    possible file conflicts.
  * Add myself to Uploaders.

 -- Mark Purcell <msp@debian.org>  Sat, 05 Jun 2010 10:48:53 +1000

kraft (0.40-0ubuntu1~ppa1) karmic; urgency=low

  * Add cdbs as build dep
  * Downgrade debhelper to version 7
  * Port rules file to cdbs and pkd-kde-tools
  * Remove debian/source
  * Change debian/compat from 7 to 5

 -- Rohan Garg <rohan16garg@gmail.com>  Wed, 14 Apr 2010 21:48:30 +0530

kraft (0.40b2+really0.40-0ubuntu1~ppa1) lucid; urgency=low

  * New upstream release
  * Remove old docbook from debian/

 -- Rohan Garg <rohan16garg@gmail.com>  Wed, 14 Apr 2010 20:41:24 +0530

kraft (0.40b2-0ubuntu1~ppa3) lucid; urgency=low

  * Updated rules file to use pkg-kde-tools
  * Added libboost1.40-dev as build dep

 -- Rohan Garg <rohan16garg@gmail.com>  Thu, 25 Mar 2010 20:14:55 +0530

kraft (0.40b2-0ubuntu1~ppa2) lucid; urgency=low

  * Added kdelibs5-dev as a build dep

 -- Rohan Garg <rohan16garg@gmail.com>  Thu, 25 Mar 2010 01:15:07 +0530

kraft (0.40b2-0ubuntu1~ppa1) lucid; urgency=low

  * New Upstream release
  * Really switch to debian 3.0 format
  * Switch to pkg-kde-tools from deprecated cdbs
  * Add kdepimlibs5-dev, libctemplate-dev, python-trml2pdf, libctemplate0, pdftk, libqt4-sql-sqlite, libkdepim4, as build deps
  * Bumped Standards version to 3.8.4
  * Changed Upstream link to  http://sourceforge.net/projects/kraft/

 -- Rohan Garg <rohan16garg@gmail.com>  Wed, 24 Mar 2010 23:41:22 +0530

kraft (0.20-0ubuntu2) karmic; urgency=low

  * kraft/src/katalogview.cpp: #include <cstdlib> for getenv.
  * kraft/src/unitmanager.cpp: add missing <cstdlib> include for abs.

 -- Stefan Potyra <sistpoty@ubuntu.com>  Tue, 29 Sep 2009 23:18:28 +0200

kraft (0.20-0ubuntu1) hardy; urgency=low

  * Initial release.

 -- Jonathan Patrick Davies <jpatrick@kubuntu.org>  Tue, 11 Dec 2007 21:17:11 +0200
